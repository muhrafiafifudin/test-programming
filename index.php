<?php
    echo 'Soal A !! <br>';
    echo 'Urutkan array berikut [12, 9, 30, "A", "M", 99, 82, "J", "N", "B"] dengan urutan abjad di depan dan angka di belakang. <br>';
    echo 'Contoh : ["A", "B", "J", "M", "N", 9, 12, 30, 82, 99] <br><br>';
    echo 'Jawab Soal A : ';

    $question_a = [12, 9, 30, "A", "M", 99, 82, "J", "N", "B"];

    function sort_array($a, $b) {
        if (is_string($a) && is_string($b)) {
            return strcmp($a, $b);
        } elseif (is_string($a) && is_numeric($b)) {
            return -1;
        } elseif (is_numeric($a) && is_string($b)) {
            return 1;
        } else {
            return $a - $b;
        }
    }

    usort($question_a, 'sort_array');

    foreach ($question_a as $key => $value) {
        if (is_string($value)) {
            echo '<strong>"' . $value . '"' . ', </strong>';
        } else {
            echo '<strong>' . $value . ', </strong>';
        }
    }

    echo '<br><br>';

    echo 'Soal B !! <br>';

    $text = 'palindrom';
    $pattern = 'ind';

    function pattern_count($text, $pattern)
    {
        $text_length = strlen($text);
        $pattern_length = strlen($pattern);
        $count = 0;

        for ($i = 0; $i <= $text_length - $pattern_length; $i++) {
            $match = true;

            for ($j = 0; $j < $pattern_length; $j++) {
                if ($text[$i + $j] != $pattern[$j]) {
                    $match = false;
                    break;
                }
            }

            if ($match) {
                $count++;
            }
        }

        return $count;
    }

    echo 'Input : "' . $text . '", "' . $pattern . '" <br>';
    echo 'Output : <strong>' . pattern_count($text, $pattern) . '</strong>';

    echo '<br><br>';

    echo 'Soal C !! <br>';

    function compare_abjad($a, $b)
    {
        $key_a = key($a);
        $key_b = key($b);

        $result = strcmp(strtolower($key_a), strtolower($key_b));

        if ($result === 0) {
            return strcmp($key_a, $key_b);
        }

        return $result;
    }

    function count_and_sort_letter($input)
    {
        $counts = [
            'lowercase' => [],
            'uppercase' => []
        ];

        $character = str_split($input);

        foreach ($character as $char) {
            if (ctype_alpha($char)) { 
                if (ctype_lower($char)) {
                    if (!isset($counts['lowercase'][$char])) {
                        $counts['lowercase'][$char] = 1;
                    } else {
                        $counts['lowercase'][$char]++;
                    }
                } else {
                    $char_lower = strtolower($char); 
                    if (!isset($counts['uppercase'][$char_lower])) {
                        $counts['uppercase'][$char_lower] = 1;
                    }
                }
            }
        }

        ksort($counts['lowercase']);
        ksort($counts['uppercase']);

        $result = [];

        foreach ($counts['lowercase'] as $char => $count) {
            $result[$char] = $count;
        }

        foreach ($counts['uppercase'] as $char_lower => $count) {
            $char_upper = strtoupper($char_lower);
            $result[$char_upper] = $count;
        }

        $output = array_map(function ($key, $value) {
            return [$key => $value];
        }, array_keys($result), $result);

        usort($output, 'compare_abjad');

        $final_result = [];

        foreach ($output as $sub_array) {
            foreach ($sub_array as $key => $value) {
                $final_result[] = '"' . $key . '"' . ':' . $value;
            }
        }

        return $final_result;
    }

    $input = "MasyaAllah";

    echo 'Input : "' . $input . '" <br>';
    echo 'Output : <strong>[' . implode(", ", count_and_sort_letter($input)) . ']</strong>';
?>